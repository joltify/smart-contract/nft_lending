/* eslint no-unused-vars: 0 */

import { ethers } from "hardhat";

import { main as deploy } from "./deploy/deploy";
import { SECTION_SEPARATOR, vaultAssetsAndMakeLoans } from "./utils/bootstrap-tools";
import { mintAndDistribute } from "./utils/mint-distribute-assets";
import { deployNFTs } from "./utils/deploy-assets";

export async function main(): Promise<void> {

    const owners = await ethers.getSigners();
    const receivers = (await ethers.getSigners()).slice(1, 7);
    const owner = owners[0]

    const balance = await  ethers.provider.getBalance(owner.getAddress())
    console.log(await owner.getAddress()+":"+balance)


    console.log(SECTION_SEPARATOR);

    for (const account of receivers){
        const tx = await owner.sendTransaction({
            to: account.address,
            value: ethers.utils.parseEther("50.0"), // Sends exactly 1.0 ether
          });


        
  const receipt = await tx.wait();
  console.log("Receipt:", receipt);

  const logs = receipt.logs;
  for (const log of logs) {
    console.log("Log:", log);
  }










        const balance = await  ethers.provider.getBalance(account.address)
        console.log(await account.getAddress()+":"+balance)
    }

    console.log(SECTION_SEPARATOR);
}

if (require.main === module) {
    main()
        .then(() => process.exit(0))
        .catch((error: Error) => {
            console.error(error);
            process.exit(1);
        });
}
