import hre from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";

import { VaultFactory, AssetVault } from "../../typechain";

type Signer = SignerWithAddress;

let vault: AssetVault | undefined;
export const createVault = async (factory: VaultFactory, user: Signer): Promise<AssetVault> => {
    const tx = await factory.connect(user).initializeBundle(await user.getAddress());
    let receipt:any
    try {
        receipt = await tx.wait();
    } catch (error) {
        console.log("failed in initialize bundle",error)
    }

    if (receipt && receipt.events) {
        for (const event of receipt.events) {
            if (event.args && event.args.vault) {
                vault = <AssetVault>await hre.ethers.getContractAt("AssetVault", event.args.vault);
            }
        }
    } else {
        throw new Error("Unable to create new vault");
    }
    if (!vault) {
        throw new Error("Unable to create new vault");
    }
    return vault;
};
