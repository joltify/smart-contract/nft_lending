#!/bin/bash

# Replace these variables with your actual values
contractAddress=$1

curl -X POST \
  -H "Content-Type: application/json" \
  --data '{
    "jsonrpc":"2.0",
    "method":"eth_getCode",
    "params":["'$contractAddress'", "latest"],
    "id":1
  }' \
  "http://127.0.0.1:8545"

