#!/bin/bash
echo $1
set -x
curl -X POST \
  -H "Content-Type: application/json" \
  --data '{
    "jsonrpc":"2.0",
    "method":"eth_getTransactionByHash",
    "params":["'$1'"],
    "id":1
  }' \
  http://127.0.0.1:8545
